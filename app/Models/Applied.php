<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Applied extends Model
{ 
    use HasFactory;

    protected $table =  "applied";
    
    protected $fillable = [
        'name', 'email', 'resume', 'cletter', 'lprofile', 'career_id'
    ];

}
