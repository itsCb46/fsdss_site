<!DOCTYPE html>
<html lang="zxx">
<head>
        <!-- meta tag -->
        <meta charset="utf-8">
        <title>FSDSS - Consulting Business</title>
        <meta name="description" content="">
        <!-- responsive tag -->
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- favicon -->
        <link rel="apple-touch-icon" href="apple-touch-icon.html">
        <link rel="shortcut icon" type="image/x-icon" href="assets/images/fav.png">
        <!-- Bootstrap v4.4.1 css -->
        <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css">
        <!-- font-awesome css -->
        <link rel="stylesheet" type="text/css" href="assets/css/font-awesome.min.css">
        <!-- animate css -->
        <link rel="stylesheet" type="text/css" href="assets/css/animate.css">
        <!-- aos css -->
        <link rel="stylesheet" type="text/css" href="assets/css/aos.css">
        <!-- owl.carousel css -->
        <link rel="stylesheet" type="text/css" href="assets/css/owl.carousel.css">
        <!-- slick css -->
        <link rel="stylesheet" type="text/css" href="assets/css/slick.css">
        <!-- off canvas css -->
        <link rel="stylesheet" type="text/css" href="assets/css/off-canvas.css">
        <!-- linea-font css -->
        <link rel="stylesheet" type="text/css" href="assets/fonts/linea-fonts.css">
        <!-- flaticon css  -->
        <link rel="stylesheet" type="text/css" href="assets/fonts/flaticon.css">
        <!-- magnific popup css -->
        <link rel="stylesheet" type="text/css" href="assets/css/magnific-popup.css">
        <!-- Main Menu css -->
        <link rel="stylesheet" href="assets/css/rsmenu-main.css">
        <!-- nivo slider CSS -->
        <link rel="stylesheet" type="text/css" href="assets/inc/custom-slider/css/nivo-slider.css">
        <link rel="stylesheet" type="text/css" href="assets/inc/custom-slider/css/preview.css">
        <!-- rsmenu transitions css -->
        <link rel="stylesheet" href="assets/css/rsmenu-transitions.css">
        <!-- spacing css -->
        <link rel="stylesheet" type="text/css" href="assets/css/rs-spacing.css">
        <!-- rs animations css -->
        <link rel="stylesheet" type="text/css" href="assets/css/rs-animations.css">
        <!-- style css -->
        <link rel="stylesheet" type="text/css" href="style.css"> <!-- This stylesheet dynamically changed from style.less -->
        <!-- responsive css -->
        <link rel="stylesheet" type="text/css" href="assets/css/responsive.css">
    </head>
    <body class="home-eleven">

        <!-- Preloader area start here -->
        <div id="loader" class="loader">
            <div class="spinner"></div>
        </div>
        <!--End preloader here -->

        <!--Full width header Start-->
        <div class="full-width-header">
            <!-- Toolbar Start -->
            <div class="toolbar-area hidden-md">
                <div class="container">
                    <div class="row y-middle">
                        <div class="col-md-5">
                            <div class="toolbar-contact">
                                <ul>
                                   <li><i class="flaticon-email"></i><a href="mailto:hr@fsdss.com">hr@fsdss.com</a></li>

                                </ul>
                            </div>
                        </div>
                       <div class="col-md-7">
                           <div class="toolbar-sl-share">
                               <ul>
                                   <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                   <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                   <li><a href="#"><i class="fa fa-pinterest-p"></i></a></li>
                                   <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                               </ul>
                           </div>
                       </div>
                    </div>
                </div>
            </div>
            <!-- Toolbar End -->
           
           <!--Header Start-->
           <header id="rs-header" class="rs-header">
               <!-- Menu Start -->
               <div class="menu-area menu-sticky">
                   <div class="container">
                       <div class="row y-middle">
                           <div class="col-lg-2">
                               <div class="logo-area">
                                   <a href="index.html"><img src="assets/images/logo-red2.png" alt="logo"></a>
                               </div>
                           </div>
                           <div class="col-lg-10 text-right">
                               <div class="rs-menu-area">
                                   <div class="main-menu">
                                       <div class="mobile-menu">
                                           <a class="rs-menu-toggle">
                                               <i class="fa fa-bars"></i>
                                           </a>
                                       </div>
                                       <nav class="rs-menu pl-160 md-pl-0">
                                           <ul class="nav-menu">
                                               <!-- current-menu-item is the class for active menu -->
                                               <li class="rs-mega-menu mega-rs"> <a href="#">Home</a>
                                               </li>

                                               <li class="">
                                                   <a href="#rs-about">About Us</a>
                                               </li>

                                               <li class="">
                                                   <a href="#service">Service</a>
                                               </li>

                                               <li class="">
                                                   <a href="#careers">Careers</a>
                                               </li>
                                               <li class="">
                                                   <a href="#rs-contact">Contact</a>
                                               </li>

                                               
                                           </ul> <!-- //.nav-menu -->
                                       </nav>
                                   </div> <!-- //.main-menu -->
                               </div>
                           </div>
                       </div>
                   </div>
               </div>
               <!-- Menu End -->
           </header>
           <!--Header End-->
        </div>
        <!--Full width header End-->

		<!-- Main content Start -->
        <div class="main-content">
            <!-- Slider Start -->
            <div id="rs-slider" class="rs-slider slider11">
                <div class="slider-carousel3 owl-carousel">
                    <!-- Slide 1 -->
                    <div class="slider slide1">
                        <div class="container">
                            <div class="row">
                                <div class="offset-lg-5"></div>
                                <div class="col-lg-7 pl-30 md-pl-15">
                                    <div class="content-part">
                                        <div class="slider-des">
                                            <h1 class="sl-title mb-21">Identifying Talent is an Art and we do it effortlessly!!!</h1>
                                            <!-- <p class="sl-desc mb-40">We are leading  professional Consult providing company<br> all over world providing services over 40 years.</p> -->
                                        </div>
                                        <div class="slider-bottom">
                                            <ul>
                                                <!-- <li><a href="contact.html" class="readon">Get In Touch</a></li> -->
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="image-part">
                                <img src="assets/images/slider/sl11-ly1.png" alt="">
                            </div>
                            <div class="sl-shape">
                                <img src="assets/images/slider/sl11-ly3.png" alt="">
                            </div>
                        </div>
                    </div>
                    <!-- Slide 2 -->
                    <div class="slider slide2">
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-8">
                                    <div class="content-part">
                                        <div class="slider-des">
                                            <h1 class="sl-title mb-21">Practice and Competency building.<br>Connect with us!!</h1>
                                            <!-- <p class="sl-desc mb-40">We are leading  professional Consult providing company<br> all over world providing services over 40 years.</p> -->
                                        </div>
                                        <div class="slider-bottom">
                                            <ul>
                                                <!-- <li><a href="contact.html" class="readon">Get In Touch</a></li> -->
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="image-part">
                                <img src="assets/images/slider/sl11-ly2.png" alt="">
                            </div>
                            <div class="sl-shape">
                                <img src="assets/images/slider/sl11-ly3.png" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Slider End -->

            <!-- About Section Start -->
            <div id="rs-about" class="rs-about style5">
                <div class="container">
                    <div class="wraping pt-120 pb-50 md-pt-72">
                        <div class="row y-middle">
                            <div class="col-lg-5 pr-20 lg-pr-15 md-mb-30">
                                <div class="sec-title mb-45 md-mb-52 sm-mb-32">
                                    <div class="sub-title red-color2">Welcome to First Soft Digital Solutions</div>
                                    <h2 class="title mb-18">We are Consulting Partner for your business </h2>
                                    <div class="desc">First Soft Digital Solutions Private Limited is a professional consulting company with competencies in IT Services, Strategy and Consulting. We are passionate team with consolidated 20+ years of experience in servicing end-to-end business solutions globally.</div>
                                </div>
                                <!-- <div class="counter-wrap">
                                    <div class="content-part mb-30">
                                        <div class="counter-part">
                                            <div class="rs-count thousand">25</div>
                                            <h4 class="title mb-0">Projects</h4>
                                        </div>
                                        <div class="desc-text">
                                            <div class="desc">On the other hand we denounce with righteous indignation and dislike men who are beguiledre</div>
                                        </div>
                                    </div>
                                    <div class="content-part">
                                        <div class="counter-part">
                                            <div class="rs-count thousand">15</div>
                                            <h4 class="title mb-0">Customer</h4>
                                        </div>
                                        <div class="desc-text">
                                            <div class="desc">All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary</div>
                                        </div>
                                    </div>
                                </div> -->
                            </div>
                            <div class="col-lg-7 pl-32 lg-pl-15">
                                <div class="img-part">
                                    <img src="assets/images/about/about11.png" alt="">
                                    <div class="morphin"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- About Section End -->
            <div class="rs-partner modify6 pt-120 pb-120 md-pt-80 md-pb-80">
                <div class="container">
                    <div class="rs-carousel owl-carousel owl-loaded owl-drag px-5" data-loop="true" data-items="4" data-margin="70" data-autoplay="false" data-hoverpause="true" data-autoplay-timeout="5000" data-smart-speed="800" data-dots="false" data-nav="false" data-nav-speed="false" data-center-mode="false" data-mobile-device="1" data-mobile-device-nav="false" data-mobile-device-dots="false" data-ipad-device="3" data-ipad-device-nav="false" data-ipad-device-dots="false" data-ipad-device2="2" data-ipad-device-nav2="false" data-ipad-device-dots2="false" data-md-device="4" data-lg-device="4" data-md-device-nav="false" data-md-device-dots="false">
                        <a class="owl-carousel__item" href="#">
                            <img class="owl-carousel__img-round img-responsive" src="{{env('APP_URL')}}/1.png" alt="company"/>
                        </a>
                        <a class="owl-carousel__item" href="#">
                            <img class="owl-carousel__img-round img-responsive" src="{{env('APP_URL')}}/2.png" alt="company"/>
                        </a>
                        <a class="owl-carousel__item" href="#">
                            <img class="owl-carousel__img-round img-responsive" src="{{env('APP_URL')}}/3.png" alt="company"/>
                        </a>
                        <a class="owl-carousel__item" href="#">
                            <img class="owl-carousel__img-round img-responsive" src="{{env('APP_URL')}}/4.png" alt="company"/>
                        </a>
                    </div>
                </div>
            </div>
            <!-- Partner Section Start -->
            <!-- Partner Section End -->

            <!-- Services Section Start -->
            <div class="rs-services style13 bg23 pt-108 pb-90 md-pt-72 md-pb-50" id="service">
                <div class="container">
                    <div class="sec-title mb-35 md-mb-51 sm-mb-31">
                        <div class="row y-middle">
                            <div class="col-lg-5 md-mb-18">
                                <div class="sub-title red-color2">Premium Services</div>
                                <h2 class="title mb-0">Our Latest Services</h2>
                            </div>
                            <!-- <div class="col-lg-7">
                                <div class="desc big pr-30 xs-pr-0">Beguiled and demoralized by the charms of pleasure of the moment so blinded by that they cannot foresee on the other hand we denounce with righteous indignation and dislike men who are so.</div>
                            </div> -->
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4 mb-30">
                            <div class="service-wrap">
                                <div class="img-part">
                                    <img src="assets/images/services/style13/1.jpg" alt="">
                                    <div class="icon-part">
                                        <i class="fa fa fa-diamond"></i>
                                    </div>
                                </div>
                                <div class="content-part">
                                    <h4 class="title"><a href="services-single.html">Cloud Services</a></h4>
                                    <div class="desc mb-4 pb-1">We help customers to build their captive cloud services. This includes Advisory services, Strategy & Road Map, Migration Services, Development and Application Modernization.</div>
                                    <div class="btn-part">
                                        <a href="services-single.html"><i class="fa fa-plus"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 mb-30">
                            <div class="service-wrap">
                                <div class="img-part">
                                    <img src="assets/images/services/style13/2.jpg" alt="">
                                    <div class="icon-part">
                                        <i class="fa fa fa-sitemap"></i>
                                    </div>
                                </div>
                                <div class="content-part">
                                    <h4 class="title"><a href="services-single.html">Digital Transformation</a></h4>
                                    <div class="desc">Our Digital transformation help business to enable business continuity and seamless customer experience.</div>
                                    <div class="btn-part">
                                        <a href="services-single.html"><i class="fa fa-plus"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 mb-30">
                            <div class="service-wrap">
                                <div class="img-part">
                                    <img src="assets/images/services/style13/3.jpg" alt="">
                                    <div class="icon-part">
                                        <i class="fa fa fa-line-chart"></i>
                                    </div>
                                </div>
                                <div class="content-part">
                                    <h4 class="title"><a href="services-single.html">Resource Consulting</a></h4>
                                    <div class="desc">We specialize in Full Time Hiring, Project Hiring and Executive Search.</div>
                                    <div class="btn-part">
                                        <a href="services-single.html"><i class="fa fa-plus"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Services Section End -->

            <!-- About Section Start -->
            <div class="rs-about style6 bg24 pt-120 pb-120 md-pt-72 md-pb-80" id="careers">
                <div class="container">
                    <div class="row y-middle">
                        <div class="col-lg-6 md-text-center">
                            <div class="img-part">
                                <img src="assets/images/about/about11-2.png" alt="">
                                <div class="morphin"></div>
                            </div>
                        </div>
                        <div class="col-lg-6 pl-80 lg-pl-15 md-mb-50 md-order-first">
                            <div class="sec-title mb-60 sm-mb-41">
                                <div class="sub-title red-color2">First Soft Digital Solution Specialization</div>
                                <h2 class="title waving-line white-color">Practice & Competency Building</h2>
                                <div class="desc white-color">We help our clients to build their captive cloud practice & competency. Our services include Consulting, Advisory, Custom Application Development, Integration, Migration and Testing.</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- About Section End -->
            <div class="rs-faq gray-bg2 inner pt-92 md-pt-72 pb-100 md-pb-80">
                <div class="container">
                    <div class="sec-title mb-31 sm-mb-11 xs-mb-31">
                        <div class="row">
                            <div class="col-lg-5 pl-40 lg-pl-15 md-mb-18">
                                <h2 class="title mb-0">Careers</h2>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12 pr-55 md-pr-15 md-mb-30">
                            <div id="accordion" class="accordion">
                                @foreach($careers as $career)
                                    <div class="card">
                                        <div class="card-header">
                                            <a class="card-link collapsed accordion-button" data-toggle="collapse" href="#collapse{{$loop->iteration}}" aria-expanded="false">{{$career->designation}}</a>
                                            <a style="float:right; margin: -50px 20px 20px 0"> <i class="fa fa-chevron-down" aria-hidden="true"></i> </a>
                                        </div>
                                        <div id="collapse{{$loop->iteration}}" class="collapse" data-parent="#accordion" style="">
                                            <div class="card-body">
                                                {!! $career->description !!}
                                                <br /><br />
                                                <button onclick="apply({{$career->id}},'{{$career->designation}}')" type="button" class="btn btn-danger text-white" style="float:right; margin: -50px 20px 20px 0">Apply</a>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Latest Project Section End -->

            <!-- Request For Proposal Section Start -->
            <div id="rs-contact" class="rs-contact style3 pt-120 md-pt-72 mb-5">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-4 md-mb-30">
                            <div class="sec-title mb-30">
                                <div class="sub-title red-color2">Contact Us</div>
                                <h2 class="title mb-0">Connect with our Specialist</h2>
                            </div>
                            <div class="address-wrap">
                                <!-- <div class="address-part mb-34"> -->
                                    <!-- <div class="icon-part">
                                        <i class="fa fa-phone"></i>
                                    </div> -->
                                    <!-- <div class="content-part">
                                        <span class="label">Phone:</span>
                                        <a href="tel:+088589-8745">(+088)589-8745</a>
                                    </div> -->
                                <!-- </div> -->
                                <div class="address-part mb-34">
                                    <div class="icon-part">
                                        <i class="fa fa-home"></i>
                                    </div>
                                    <div class="content-part">
                                        <span class="label">Email:</span>
                                        <a href="mailto:hr@rstheme.com">sri@fsdss.com</a>
                                    </div>
                                </div>
                                <div class="address-part">
                                    <div class="icon-part">
                                        <i class="fa fa-map-marker"></i>
                                    </div>
                                    <div class="content-part">
                                        <span class="label">Address:</span>
                                        <div class="desc">951, 24th Main Road, R.K. Colony, Marenahalli 1st Phase J.P. Nagar Bangalore -560076, Karnataka. India</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-8">
                            <form class="contact-form" method="POST" action="https://formsubmit.co/sri@fsdss.com">
                                <input type="hidden" name="_next" value="{{env('APP_URL')}}">
                                <div class="row">
                                    <div class="col-md-6 mb-30">
                                        <input type="text" name="name" placeholder="Name" required="">
                                    </div>
                                    <div class="col-md-6 mb-30">
                                        <input type="email" name="email" placeholder="E-mail" required="">
                                    </div>
                                    <div class="col-md-6 mb-30">
                                        <input type="text" name="phone" placeholder="Phone Number" required="">
                                    </div>
                                    <div class="col-md-6 mb-30">
                                        <input type="text" name="website" placeholder="Your Website">
                                    </div>
                                    <div class="col-12 mb-30">
                                        <textarea placeholder="Your Message Here" required=""></textarea>
                                    </div>
                                    <div class="col-12">
                                        <button type="submit">Submit Now</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Request For Proposal Section End -->

            <!-- Call to Action Section Start -->
            <!-- <div class="rs-cta style2 pt-100 pb-100 md-pt-80 md-pb-80">
                <div class="cta-wrap text-center">
                    <h2 class="title">Let's Start Your Next Project With Us</h2>
                    <div class="btn-part">
                        <a class="readon" href="contact.html">Contact Now</a>
                    </div>
                </div>
            </div> -->
            <!-- Call to Action Section End -->

            <!-- Testimonial Section Start -->
            <!-- Testimonial Section End -->
            

            <!-- Apply Modal Start -->
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalCenterTitle">Application Form</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form method="POST" action="{{env('APP_URL')}}/applyJob" enctype='multipart/form-data'>
                            <div class="row">
                                <div class="col-lg-12 col-md-12">
                                    <div class="billing-fields">
                                        <div class="checkout-title">
                                            <h3 id="designationApplied"></h3>
                                        </div>
                                        <div class="form-content-box">
                                            <div class="row">
                                                {{csrf_field()}}
                                                <input type="hidden" id="applied_id" name="careerId">
                                                <div class="col-md-12 col-sm-12 col-xs-12">
                                                    <div class="form-group">
                                                        <label>Name <span>*</span></label>
                                                        <input id="name" name="name" class="form-control" type="text" required="required">
                                                    </div>
                                                </div>
                                                <div class="col-md-12 col-sm-12 col-xs-12">
                                                    <div class="form-group">
                                                        <label>Email <span>*</span></label>
                                                        <input id="email" name="email" class="form-control" type="email" required="required">
                                                    </div>
                                                </div>
                                                <div class="col-md-12 col-sm-12 col-xs-12">
                                                    <div class="form-group">
                                                        <label>Resume <span>*</span></label>
                                                        <div class="custom-file">
                                                            <input type="file" name="resume" class="custom-file-input" id="inputGroupFile01" aria-describedby="inputGroupFileAddon01">
                                                            <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- <div class="col-md-12 col-sm-12 col-xs-12">
                                                    <div class="form-group">
                                                        <label>Linkedin Profile</label>
                                                        <input id="lprofile" name="lprofile" class="form-control" type="text">
                                                    </div>
                                                </div> -->
                                                <div class="col-md-12 col-sm-12 col-xs-12">
                                                    <div class="form-group">
                                                        <label>Cover Letter</label>
                                                        <textarea id="cover_letter" name="cletter" class="form-control"> </textarea>
                                                    </div>
                                                </div>
                                                <input type="submit" class="invisible" id="career_submit">
                                            </div>
                                        </div>
                                    </div><!-- .billing-fields -->
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary" onclick="career_submit()">Submit</button>
                    </div>
                    </div>
                </div>
            </div>
            <!-- Apply Modal End -->

        </div> 
        <!-- Main content End -->

        <!-- Footer Start -->
        <footer id="rs-footer" class="rs-footer">
            <div class="bg-wrap">
                <div class="container">
                    <div class="footer-bottom">
                        <div class="row y-middle">
                            <div class="col-lg-6 col-md-8 sm-mb-21">
                                <div class="copyright">
                                    <p>© Copyright 2021 FSDS. All Rights Reserved.</p>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-4 text-right sm-text-center">
                                <img src="assets/images/msme.jpg" style="width: 100px" alt="logo">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!-- Footer End -->

        <!-- start scrollUp  -->
        <div id="scrollUp">
            <i class="fa fa-angle-up"></i>
        </div>
        <!-- End scrollUp  -->

        <!-- modernizr js -->
        <script src="assets/js/modernizr-2.8.3.min.js"></script>
        <!-- jquery latest version -->
        <script src="assets/js/jquery.min.js"></script>
        <!-- Bootstrap v4.4.1 js -->
        <script src="assets/js/bootstrap.min.js"></script>
        <!-- Menu js -->
        <script src="assets/js/rsmenu-main.js"></script> 
        <!-- op nav js -->
        <script src="assets/js/jquery.nav.js"></script>
        <!-- owl.carousel js -->
        <script src="assets/js/owl.carousel.min.js"></script>
        <!-- Slick js -->
        <script src="assets/js/slick.min.js"></script>
        <!-- isotope.pkgd.min js -->
        <script src="assets/js/isotope.pkgd.min.js"></script>
        <!-- imagesloaded.pkgd.min js -->
        <script src="assets/js/imagesloaded.pkgd.min.js"></script>
        <!-- wow js -->
        <script src="assets/js/wow.min.js"></script>
        <!-- aos js -->
        <script src="assets/js/aos.js"></script>
        <!-- Skill bar js -->
        <script src="assets/js/skill.bars.jquery.js"></script>
        <script src="assets/js/jquery.counterup.min.js"></script>         
         <!-- counter top js -->
        <script src="assets/js/waypoints.min.js"></script>
        <!-- video js -->
        <script src="assets/js/jquery.mb.YTPlayer.min.js"></script>
        <!-- magnific popup js -->
        <script src="assets/js/jquery.magnific-popup.min.js"></script>
        <!-- Nivo slider js -->
        <script src="assets/inc/custom-slider/js/jquery.nivo.slider.js"></script>
        <!-- plugins js -->
        <script src="assets/js/plugins.js"></script>
        <!-- parallax-effect js -->
        <script src="assets/js/parallax-effect.min.js"></script>
        <!-- contact form js -->
        <script src="assets/js/contact.form.js"></script>
        <!-- ProgressBar Js -->
        <script src="assets/js/jQuery-plugin-progressbar.js"></script>
        <!-- main js -->
        <script src="assets/js/main.js"></script>
        <script>
            function apply(id,name) {
                $("#designationApplied").text(name);
                $("#applied_id").val(id);
                $('#myModal').modal("show");
            }
            function career_submit() {
                $("#career_submit").click();
            }
            @if (\Session::has('success'))
                alert("Successfully Applied! Will contact you shrotly");
            @endif
        </script>
    </body>

</html>